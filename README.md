# OpenML dataset: scene

https://www.openml.org/d/40595

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multi-label dataset. The scene dataset is an image classification task where labels like Beach, Mountain, Field, Urban are assigned to each image.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40595) of an [OpenML dataset](https://www.openml.org/d/40595). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40595/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40595/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40595/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

